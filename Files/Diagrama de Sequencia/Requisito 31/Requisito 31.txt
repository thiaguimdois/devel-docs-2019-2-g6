@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Posiciona a pe�a no topo de outra pe�a.
Cliente -> Cliente: Prepara o comando: f < iDJogador > < tipoPeca >< posicao > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: f < iDJogador > < tipoPeca >< posicao >  em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Configura a pe�a na posi��o informada pelo servidor
Cliente -> Jogador : Mostra no tabuleiro onde a pe�a foi salva!

@enduml
