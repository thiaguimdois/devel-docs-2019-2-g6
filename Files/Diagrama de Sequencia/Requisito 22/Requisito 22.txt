@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Moderador solicita pausar/despausar o jogo.
Cliente -> Cliente: Prepara o comando: pause < pause >< iDJogador > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: pause < pause > em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Pausa a partida.
Cliente -> Jogador : Notifica no batepapo que a partida est� pausada/Despausada.

@enduml
