@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Jogador aplica o especial em algum jogador.
Cliente -> Cliente: Prepara o comando:sb < iDJogadorAlvo > < especial > < iDJogadorOrigem > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: sb < iDJogadorAlvo > < especial > < iDJogadorOrigem > em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : O cliente alvo do especial, aplica o especial no tabuleiro do seu jogador
Cliente -> Jogador : Altera as pe�as no tabuleiro do jogador.

@enduml
